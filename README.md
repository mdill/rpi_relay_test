# Raspberry Pi 4-channel relay test

## Purpose

This simple sketch is designed to test a 4-channel relay by cycling through each
module with a series of patterns.  Each cycle will increase/decrease speed after
each cycle.

To end the test, use a keyboard interrupt: `Ctrl + c`

## Downloading

    cd ~/
    git clone https://bitbucket.org/mdill/rPi_relay_test.git

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/rpi_relay_test/src/e3baabebb977f370ae94a84782ac6947a5abce97/LICENSE.txt?at=master) file for
details.

